import {useEffect, useState} from 'react'

function UseEffectStateDemo() {
  const [count, setCount] = useState(0)

  // useEffect is a callback function that gets called on every render
  useEffect(() => {
    document.title = `You clicked ${count} times`
  })

  function handleAlertClick() {
    setTimeout(() => {
      alert('You clicked on: ' + count)
    }, 3000)
  }

  return (
    <>
      <p>You clicked {count} times</p>
      <button onClick={() => setCount(count + 1)}>
        Count +1
      </button>
      <button onClick={handleAlertClick}>
        Show Alert
      </button>
    </>
  )
}

export default UseEffectStateDemo