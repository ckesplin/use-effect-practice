import {useEffect, useState} from 'react'

function UseEffectWithDelay() {
  const [count, setCount] = useState(0)

  //even if the effect is executed after the render in which it was executed has been cleaned up, React
  // still remembers the state of the component at the time of execution
  useEffect(() => {
    setTimeout(() => {
      console.log(`You clicked ${count} times`)
    }, 3000)
  })


  return (
    <>
      <p>You clicked {count} times</p>
      <button onClick={() => setCount(count + 1)}>
        Count +1
      </button>
    </>
  )
}

export default UseEffectWithDelay