import {useEffect, useRef, useState} from 'react'

function UseRefExample() {
  const [count, setCount] = useState(0)
  //useRef will give you access to the latest state value in useEffect so that it doesn't use the state
  //value when the effect was executed
  const latestCount = useRef(count)

  useEffect(() => {
    latestCount.current = count
    setTimeout(() => {
      console.log(`You clicked ${latestCount.current} times`)
    }, 3000)
  })


  return (
    <>
      <p>You clicked {count} times</p>
      <button onClick={() => setCount(count + 1)}>
        Count +1
      </button>
    </>
  )
}

export default UseRefExample