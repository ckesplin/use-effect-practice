import {useEffect, useState} from 'react'

function UseEffectCleanup() {
  const [count, setCount] = useState(0)

  useEffect(() => {
    console.log(`You clicked ${count} times`)
    return (() => console.log(`The component with ${count} clicks unmounted`)
    )
  })

  return (
    <>
      <p>You clicked {count} times</p>
      <button onClick={() => setCount(count + 1)}>
        Count +1
      </button>
    </>
  )
}

export default UseEffectCleanup