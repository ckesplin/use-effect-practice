import React, { useReducer, useEffect } from "react";

const initialState = {
  count: 0,
  step: 1,
  version: 1
};

export default function ReducerDemo() {
  const [state, dispatch] = useReducer(reducer, initialState);
  const { count, step, version } = state;

  useEffect(() => {
    const id = setInterval(() => {
      dispatch({ type: 'tick' });
    }, 1000);
    return () => clearInterval(id);
  }, [dispatch]);  //show example that you can remove dispatch without a warning

  return (
    <div>
      <h1>{count}, version v{version}</h1>
      <input value={step} onChange={e => {
        dispatch({
          type: 'step',
          step: Number(e.target.value),
        });
      }} />
    </div>
  );
}

function reducer(state, action) {
  const { count, step, version } = state;
  if (action.type === 'tick') {
    // console.log(state.version)  // show that you must pass the state completely
    return {
      count: count + step,
      step,
      version //show that you must pass the state completely
    };
  } else if (action.type === 'step') {
    return { count, step: action.step, version: version + 1 };
  } else {
    throw new Error();
  }
}