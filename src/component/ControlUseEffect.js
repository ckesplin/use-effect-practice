import {useEffect, useState} from 'react'

function UseEffectStateDemo(props) {
  const [count, setCount] = useState(0)

  useEffect(() => {
    document.title = props.name
    console.log("useEffect tapped")
  }
  // A deps array will compare new values with the previous render's values.  If they have changed, it executes the effect.
  // , [props.name]
  )

  return (
    <>
      <p>You clicked {count} times</p>
      <button onClick={() => setCount(count + 1)}>
        Count +1
      </button>
    </>
  )
}

export default UseEffectStateDemo