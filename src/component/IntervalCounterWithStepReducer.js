import React, {useEffect, useReducer} from 'react'

function IntervalCounterWithStepReducer( props ) {
  const [count, dispatch] = useReducer(reducer, 0)
  // const [otherCount, dispatch2] = useReducer(reducer, 10)

  function reducer(count, action) {
    if (action.type === 'tick') {
      return count + props.step
    } else {
      throw new Error()
    }
  }

  useEffect(() => {
    const id = setInterval(() => {
      dispatch({ type: 'tick' })
      // dispatch2({type: 'tick'})
    }, 1000);
    return () => clearInterval(id)
    // dispatch isn't necessary to include in deps, but can be included
  }, [])

  return (
    <>
      <h1>{`Count ${count}`}</h1>
      {/*<h1>{`Other Count ${otherCount}`}</h1>*/}
    </>
  )
}

export default IntervalCounterWithStepReducer