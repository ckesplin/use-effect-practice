import {useState} from 'react'

function StateDemo() {
  const [count, setCount] = useState(0)

  function handleAlertClick() {
    setTimeout(() => {
      alert('You clicked on: ' + count);
    }, 3000);
  }

  return (
    <>
      <p>You clicked {count} times</p>
      <button onClick={() => setCount(count + 1)}>
        Count +1
      </button>
      <button onClick={handleAlertClick}>
        Show Alert
      </button>
    </>
  );
}

export default StateDemo;
