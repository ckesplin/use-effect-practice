import React, {useEffect, useState} from 'react'

function UseEffectStateDemo() {
  const [count, setCount] = useState(1)

  useEffect(() => {
    const id = setInterval(() => {
      setCount(count + 1)
    }, 1000)
    return () => clearInterval(id)
  },
    // don't lie about the deps
    // []
    [count]
  )

  return <h1>{count}</h1>
}

export default UseEffectStateDemo