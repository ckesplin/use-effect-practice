import {useEffect, useState} from 'react'
import Hello from './Hello'

function Clicker() {
  const [count, setCount] = useState(0);
  // let name = "Carson"
  const [name, setName] = useState("")
  // const latestCount = useRef(null)
  // const [name, setName] = useState('bob')

  // function sleep(ms) {
  //   const date = Date.now()
  //   let currentDate = null
  //   do {
  //     currentDate = Date.now()
  //   } while (currentDate - date < ms)
  // }

  useEffect(() => {
    document.title = `You clicked ${count} times`;
    console.log("useEffect")
    // return(() => console.log("Count after cleanup = " + count))
  }, [count]);

  useEffect(() => {
    const id = setInterval(() => {
      setCount(c => c + 1);
    }, 1000);
    return () => clearInterval(id);
  }, []);


  // useEffect(() => {
  //   // Set the mutable latest value
  //   latestCount.current = count;
  //   setTimeout(() => {
  //     // Read the mutable latest value
  //     console.log(`You clicked ${latestCount.current} times`);
  //   }, 3000);
  // });

  function handleClick(inc) {
    setCount(countX => countX + inc)

  }

  function handleAlertClick() {
    // sleep(3000)
    setTimeout(() => {
      // setCount(count + 1)
      alert(`You clicked on ${count}`)
    }, 3000);
  }

  function handleChangeNameClick() {
    console.log(name)
    return (
      name === "Carson" ? setName("Jose") : setName("Carson")
      // name === "Carson" ? name = "Jose" : name = "Carson"
    )
  }

  return (
    <div className="App">
      <p>You clicked {count} times</p>
      <button onClick={() => handleClick(1)}>
        Click me
      </button>
      <button onClick={() => handleChangeNameClick()}>
        Change Name
      </button>
      <button onClick={handleAlertClick}>
        Show alert
      </button>
      <Hello name={name} />
    </div>
  );
}

export default Clicker;
