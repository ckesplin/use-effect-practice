import React, {useEffect, useState} from 'react'
import axios from 'axios'

function MoveFunctionInside(props) {
  const [response, setResponse] = useState([])

  useEffect(() => {
    function getWorkorder(workorderId) {
      return  axios.request({
        url: `http://localhost:8090/api/v3/workorders/${workorderId}`,
        method: "GET",
        headers: {
          'Accept': 'application/json',
        },
      }).then(response => response.data)
        .catch(error => {
          return Promise.reject(error.response)
        })
    }
    getWorkorder(props.workOrderId).then(res => console.log(res))

  }, [])

  return (
    <div>
      <div>HIIII</div>
      <div>{response}</div>
    </div>
  )
}

export default MoveFunctionInside