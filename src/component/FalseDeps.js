import React, {useEffect, useState} from 'react'

function FalseDeps() {
  const [count, setCount] = useState(1)

  useEffect(() => {
      setInterval(() => {
        //Reference the value of the state rather than the state
        //React knows the value of count and it will be static each time, so you can pass the state by reference
        //instead of by value.  No warnings shown.

        // Yes, count was a necessary dependency because we wrote setCount(count + 1) inside the effect. However,
        // we only truly needed count to transform it into count + 1 and “send it back” to React. But React already
        // knows the current count. All we needed to tell React is to increment the state — whatever it is right now.
        setCount(c => c + 1 )
      }, 1000)
    }, [])

  return <h1>{count}</h1>
}

export default FalseDeps