import './App.css'
import React, {useState} from 'react'
import StateDemo from './component/StateDemo'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom"
import UseEffectStateDemo from './component/useEffectStateDemo'
import UseEffectWithDelay from './component/UseEffectWithDelay'
import UseRefExample from './component/UseRefExample'
import UseEffectCleanup from './component/UseEffectCleanup'
import ControlUseEffect from './component/ControlUseEffect'
import LyingToUseEffect from './component/LyingToUseEffect'
import FalseDeps from './component/FalseDeps'
import IntervalCounterWithStep from './component/IntervalCounterWithStep'
import IntervalCounterWithStepReducer from './component/IntervalCounterWithStepReducer'
import MoveFunctionInside from './component/MoveFunctionInside'

function App() {
  const [step, setStep] = useState(1)

  return (
    <Router>
      <div className="App">
          <ul>
            <div className="container">
              <li>
                <Link to="/1">StateDemo</Link>
              </li>
              <li>
                <Link to="/2">useEffect State Demo</Link>
              </li>
              <li>
                <Link to="/3">useEffect With Delay</Link>
              </li>
              <li>
                <Link to="/4">useRef</Link>
              </li>
              <li>
                <Link to="/5">useEffect Cleanup</Link>
              </li>
              <li>
                <Link to="/6">Controlling useEffect</Link>
              </li>
              <li>
                <Link to="/7">Lying to useEffect</Link>
              </li>
              <li>
                <Link to="/8">Handling "False Deps"</Link>
              </li>
              <li>
                <Link to="/9">Counter With Steps</Link>
              </li>
              <li>
                <Link to="/10">Counter With Steps and useReducer</Link>
              </li>
              <li>
                <Link to="/11">Move Functions Inside useEffect</Link>
              </li>
            </div>
          </ul>
        <hr />

        <Switch>
          <Route exact path="/">
            <h1>Welcome to the Jose and Carson useEffect Show!!!</h1>
          </Route>
          <Route exact path="/1">
            <StateDemo />
          </Route>
          <Route exact path="/2">
            <UseEffectStateDemo />
          </Route>
          <Route exact path="/3">
            <UseEffectWithDelay />
          </Route>
          <Route exact path="/4">
            <UseRefExample />
          </Route>
          <Route exact path="/5">
            <UseEffectCleanup />
          </Route>
          <Route exact path="/6">
            <ControlUseEffect name={"Willis J. Flapjack"} />
          </Route>
          <Route exact path="/7">
            <LyingToUseEffect />
          </Route>
          <Route exact path="/8">
            <FalseDeps />
          </Route>
          <Route exact path="/9">
            <IntervalCounterWithStep />
          </Route>
          <Route exact path="/10">
            <IntervalCounterWithStepReducer step={step}/>
            <input value={step} onChange={e => setStep(Number(e.target.value))} />
          </Route>
          <Route exact path="/11">
            <MoveFunctionInside workOrderId = 'c6c96671-102b-4fc0-b99b-e5e373ba6171'/>
          </Route>
        </Switch>
      </div>
    </Router>
  )
}

export default App
