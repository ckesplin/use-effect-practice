import axios from 'axios'

export function getWorkorder(workorderId) {
  return  axios.request({
    url: `http://localhost:8090/api/v3/workorders/${workorderId}`,
    method: "GET",
    headers: {
      'Accept': 'application/json',
    },
  }).then(response => response.data)
    .catch(error => {
      return Promise.reject(error.response)
    })
}